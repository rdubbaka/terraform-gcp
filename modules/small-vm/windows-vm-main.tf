data "template_file" "windows-metadata" {
template = <<EOF
# Install IIS
Install-WindowsFeature -name Web-Server -IncludeManagementTools;
EOF
}

resource "google_compute_instance" "vm_instance_public" {
  count = length(var.vmtype)
  name = "${var.prefix}-${var.vmtype[count.index]}"
  machine_type = var.windows_instance_type
  zone         = var.gcp_zone
  hostname     = "${var.prefix}-${var.vmtype[count.index]}.optelcloud.com"
  tags         = ["rdp","http"]
  boot_disk {
    initialize_params {
      image = var.windows_2022_sku
    }
  }
  attached_disk {
    source = "${element(google_compute_disk.win-vm-disk-.*.self_link, count.index)}"
    device_name = "${element(google_compute_disk.win-vm-disk-.*.name, count.index)}"

  }
  metadata = {
    sysprep-specialize-script-ps1 = data.template_file.windows-metadata.rendered
  }
  network_interface {
    network       = google_compute_network.vpc.name
    subnetwork    = google_compute_subnetwork.network_subnet.name
    access_config { }
  }
}

resource "google_compute_disk" "win-vm-disk-" {
  count = length(var.vmtype)
  name = "win-vm-disk-${var.prefix}-${var.vmtype[count.index]}"
  type = "pd-ssd"
  zone = var.gcp_zone
  physical_block_size_bytes = var.disk_size
}


