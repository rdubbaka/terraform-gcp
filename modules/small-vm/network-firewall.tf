# Allow http
resource "google_compute_firewall" "allow-http" {
  name    = "optelcloud-fw-allow-http"
  network = google_compute_network.vpc.name
  allow {
    protocol = "tcp"
    ports    = ["80"]
  }
  
  source_ranges = var.source_address_ranges
  target_tags = ["http"] 
}
# allow rdp
resource "google_compute_firewall" "allow-rdp" {
  name    = "optelcloud-fw-allow-rdp"
  network = google_compute_network.vpc.name
  allow {
    protocol = "tcp"
    ports    = ["3389"]
  }
  source_ranges = var.source_address_ranges
  target_tags = ["rdp"]
}

# rule for appvm
resource "google_compute_firewall" "allow-appvm-port" {
  name  = var.appvm_rule_name
  network = google_compute_network.vpc.name
  allow {
    protocol = "tcp"
    ports    = var.application_ports
  }
  source_ranges = var.source_address_ranges
  target_tags = ["applicationport"]
}
