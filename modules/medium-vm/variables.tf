variable "network_subnet_cidr" {
  type        = string
  description = "The CIDR for the network subnet"
}

variable "network_name" {
  type = string
  description  = "name of the network"
  default = "gcp-win-vnet"
}

variable "windows_instance_type" {
  type        = string
  description = "VM instance type for Windows Server"
  default     = "n2-standard-2"
}

variable "node_count" {
  type = number
  default = "1"
}

variable "prefix" {
  type = string
}

variable "disk_size" {
  default = "4096"
}
variable "gcp_region" {
  type = string
  default = "us-west4"
}

variable "gcp_zone" {
  type = string
  default = "us-west4-b"
}

variable "vmtype" {
  type = list(string)
  default = ["appvm" ,"dbvm"]
}

variable "application_ports" {
  default = ["443","8443","80","8080","5985","15672","5432","5672","8081","10943","27017"]
}

variable "source_address_ranges" {
  default = ["20.97.221.235","70.82.134.12","173.242.179.160/27","159.134.255.116/30","49.248.77.108/30","103.97.95.176/30","187.32.175.192/29","99.243.240.143","72.141.42.69","173.179.139.143"]
}

variable "dev_source_address_prefixes" {
  default = ["20.122.38.17","99.243.240.143"]
}

variable "appvm_rule_name" {
  default = "appvm-fw-allow-rule"
}
