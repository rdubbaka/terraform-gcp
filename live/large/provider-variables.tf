# GCP authentication file
variable "gcp_auth_file" {
  type        = string
  description = "GCP authentication file"
}
# define GCP region
variable "gcp_region" {
  type        = string
  description = "GCP region"
}
# define GCP project name
variable "gcp_project" {
  type        = string
  description = "GCP project name"
}

variable "gcp_zone" {
  type        = string
  description = "GCP zone name"
}

variable "company" {
  type        = string
  description = "company name"
}

variable "app_name" {
  type        = string
  description = "GCP app name"
}

variable "app_domain" {
  type        = string
  description = "GCP app domain name"
}

variable "environment" {
  type        = string
  description = "GCP env name"
}

