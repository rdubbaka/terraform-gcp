# GCP Settings
gcp_project   = "thermal-highway-367123"
gcp_region    = "us-west4"
gcp_auth_file = "/tmp/key.json"
gcp_zone      = "us-west4-b"

# Application Definition 
company     = "optelcloud"
app_name    = "optelcloud"
app_domain  = "optelcloud.com"
environment = "dev" # Dev, Test, Prod, etc

# GCP Netwok
network_subnet_cidr = "10.10.15.0/24"

# Windows VM
windows_instance_type = "n2-standard-2"
