module "deployment" {
  source = "../../modules/large-vm"
  prefix = var.prefix
  network_subnet_cidr = var.network_subnet_cidr
}
