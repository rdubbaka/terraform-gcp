terraform {
  backend "http" {
    address= "https://gitlab.com/api/v4/projects/40962655/terraform/state/terraform-gcp" 
    lock_address= "https://gitlab.com/api/v4/projects/40962655/terraform/state/terraform-gcp/small/lock" 
    unlock_address= "https://gitlab.com/api/v4/projects/40962655/terraform/state/terraform-gcp/small/lock"
    username= "rdubbaka"
    password= "TOKEN"
    lock_method= "POST"
    unlock_method= "DELETE" 
    retry_wait_min= "5"
  }
}
