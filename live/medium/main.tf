module "deployment" {
  source = "../../modules/medium-vm"
  prefix = var.prefix
  network_subnet_cidr = var.network_subnet_cidr
  network_name = var.network_name
}
