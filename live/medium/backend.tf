terraform {
  backend "http" {
    address= "https://gitlab.com/api/v4/projects/40962655/terraform/state/PREFIX" 
    lock_address= "https://gitlab.com/api/v4/projects/40962655/terraform/state/PREFIX/lock" 
    unlock_address= "https://gitlab.com/api/v4/projects/40962655/terraform/state/PREFIX/lock"
    username= "rdubbaka"
    password= "TOKEN"
    lock_method= "POST"
    unlock_method= "DELETE" 
    retry_wait_min= "5"
  }
}
